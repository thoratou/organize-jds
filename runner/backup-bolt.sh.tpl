FOLDER=/etc/organize-jds/bolt
TARGET_FOLDER={{ JDS_CONTAINER_NAME }}.backup/`date "+%Y.%m.%d_%H-%M-%S"`
TARGET_ROOT_FOLDER={{ JDS_CONTAINER_NAME }}.backup

/bin/skicka mkdir $TARGET_ROOT_FOLDER
/bin/skicka mkdir $TARGET_FOLDER
/bin/skicka upload $FOLDER/bolt.db $TARGET_FOLDER
