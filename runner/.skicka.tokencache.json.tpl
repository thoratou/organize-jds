{
  "ClientId": "{{ SKICKA_CLIENT_ID }}",
  "access_token": "{{ SKICKA_ACCESS_TOKEN }}",
  "token_type": "Bearer",
  "refresh_token": "{{ SKICKA_REFRESH_TOKEN }}",
  "expiry": "{{ SKICKA_EXPIRY }}"
}