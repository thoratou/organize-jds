DOCKER_ID = $(shell docker ps | grep "organize-jds-builder" | awk '{print $$1}')
VSCODE_ID = $(shell docker ps | grep "vscode-go" | awk '{print $$1}')
ORGANIZE_JDS_ROOT = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SSH_DIR = $(shell dirname $$SSH_AUTH_SOCK)

.PHONY: builder

builder: build-builder start-builder bash-builder

stop-all: stop-builder stop-vscode

# Local builder

build-builder:
	@docker build -f builder/Dockerfile -t organize-jds-builder .

start-builder:
	@if [ -z "$(DOCKER_ID)" ]; then \
		docker run \
		--rm \
		-it \
		--name organize-jds-builder \
		--env SSH_AUTH_SOCK=$$SSH_AUTH_SOCK \
		--shm-size=512m \
		-v gopath-jds:/go \
		-v $(ORGANIZE_JDS_ROOT):/go/src/gitlab.com/thoratou/organize-jds \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-p 8081:80 \
		organize-jds-builder; \
	else \
		echo "container already started: $(DOCKER_ID)"; \
	fi

bash-builder:
	@if [ -z "$(DOCKER_ID)" ]; then \
		echo "container not started"; \
	else \
		docker exec -it $(DOCKER_ID) bash; \
	fi

stop-builder:
	@if [ -z "$(DOCKER_ID)" ]; then \
		echo "container already stopped"; \
	else \
		docker stop $(DOCKER_ID); \
	fi

push-builder:
	@docker login registry.gitlab.com/thoratou; \
	docker tag organize-jds-builder registry.gitlab.com/thoratou/organize-jds/builder; \
	docker push registry.gitlab.com/thoratou/organize-jds/builder


# Visual Code

start-vscode:
	@if [ -z "$(VSCODE_ID)" ]; then \
		docker run \
		--rm \
		-d \
		--name vscode-go \
		--env SSH_AUTH_SOCK=$$SSH_AUTH_SOCK \
		--shm-size=512m \
		-v gopath-jds:/go \
		-v $(ORGANIZE_JDS_ROOT):/go/src/gitlab.com/thoratou/organize-jds \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		-v $(SSH_DIR):$(SSH_DIR) \
		-v /var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket \
		registry.gitlab.com/thoratou/docker-vscode-go; \
	else \
		echo "container already started: $(VSCODE_ID)"; \
	fi

launch-vscode:
	@if [ -z "$(VSCODE_ID)" ]; then \
		echo "container not started"; \
	else \
		xhost +local:docker; \
		docker exec \
		-it \
		--env DISPLAY=$$DISPLAY \
		--env ADD_GOPATH=/go \
		$(VSCODE_ID) \
		/bin/sh -c 'export GOPATH="$$GOPATH:$$ADD_GOPATH" && code /go/src/gitlab.com/thoratou/organize-jds --disable-gpu --user-data-dir=/vscode/'; \
	fi

stop-vscode:
	@if [ -z "$(VSCODE_ID)" ]; then \
		echo "container already stopped"; \
	else \
		docker stop $(VSCODE_ID); \
	fi

code: start-vscode launch-vscode
