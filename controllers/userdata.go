package controllers

import (
	"encoding/json"

	"github.com/astaxie/beego/logs"
	"github.com/boltdb/bolt"
	"gitlab.com/thoratou/organize-jds/models"
)

//DeserializeCurrentPlayerIDFromDB deserialize current player ID from mail
func DeserializeCurrentPlayerIDFromDB(bucket *bolt.Bucket, mail string) (string, error) {
	v := bucket.Get([]byte(mail))
	userData := &models.UserData{}
	err := json.Unmarshal(v, userData)
	logs.Info("mail:", mail)
	logs.Info("userData:", userData)
	logs.Info("playerID:", userData.PlayerID)
	return userData.PlayerID, err
}
