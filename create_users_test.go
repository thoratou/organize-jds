package main_test

import (
	"encoding/json"
	"log"
	"os"
	"testing"
	"time"

	"github.com/boltdb/bolt"
	"gitlab.com/thoratou/organize-jds/controllers"
	"gitlab.com/thoratou/organize-jds/models"
)

func TestCreateUsers(t *testing.T) {
	err := os.MkdirAll("bolt", 0755)
	if err != nil {
		log.Fatal(err)
		return
	}
	db, err := bolt.Open("bolt/bolt.db", 0600, &bolt.Options{Timeout: 10 * time.Second})
	if err != nil {
		log.Fatal(err)
		return
	}

	db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucketIfNotExists([]byte("users"))
		tx.CreateBucketIfNotExists([]byte("players"))

		createDefaultUser(tx, "toto1@gmail.com")
		createDefaultUser(tx, "toto2@gmail.com")
		createDefaultUser(tx, "toto3@gmail.com")
		return nil
	})
}

func createDefaultUser(tx *bolt.Tx, mail string) {
	userData := models.UserData{}

	users := tx.Bucket([]byte("users"))
	existingData := users.Get([]byte(mail))
	if existingData != nil {
		if err := json.Unmarshal(existingData, &userData); err != nil {
			return
		}
	} else {
		players := tx.Bucket([]byte("players"))
		userData.PlayerID = controllers.CreatePlayer(players, mail)
	}

	userData.SHAPassword = controllers.ConvertToSHA1("default")

	//put user data in all cases to at least reset password
	updatedUser, _ := json.Marshal(userData)
	users.Put([]byte(mail), updatedUser)
}
