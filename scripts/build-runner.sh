#!/bin/bash

go build -o organize-jds

envtpl --keep-template settings.json.tpl
envtpl --keep-template runner/backup-bolt.sh.tpl
envtpl --keep-template runner/.skicka.tokencache.json.tpl

cp /go/bin/skicka skicka

docker build . -f runner/Dockerfile -t $JDS_CONTAINER_NAME

rm skicka
