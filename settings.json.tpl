{
    "site" : "{{ JDS_SITE }}",
    "css" : "{{ JDS_CSS }}",
    "companyName" : "{{ JDS_COMPANY_NAME }}",
    "mailExtension" : "{{ JDS_COMPANY_MAIL_EXTENSION }}",
    "senderMail" : "{{ JDS_SENDER_MAIL }}",
    "senderPassword" : "{{ JDS_SENDER_PASSWORD }}",
    "contact" : "{{ JDS_CONTACT }}",
    "logo" : "{{ JDS_COMPANY_LOGO }}"
}
