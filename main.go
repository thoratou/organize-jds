package main

import (
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
	"gitlab.com/thoratou/organize-jds/controllers"
)

func main() {
	//open settings
	settings, err := controllers.DeserializeCompanyFromJSONFile("./settings.json")
	if err != nil {
		log.Fatal(err)
		return
	}

	controllers.SetSettings(settings)

	//open and feed DB if required
	err = os.MkdirAll("bolt", 0755)
	if err != nil {
		log.Fatal(err)
		return
	}
	db, err := bolt.Open("bolt/bolt.db", 0600, &bolt.Options{Timeout: 10 * time.Second})
	if err != nil {
		log.Fatal(err)
		return
	}

	db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucketIfNotExists([]byte("users"))
		tx.CreateBucketIfNotExists([]byte("teams"))
		tx.CreateBucketIfNotExists([]byte("players"))
		tx.CreateBucketIfNotExists([]byte("forumids"))

		b := tx.Bucket([]byte("games"))
		if b == nil {
			createdBucket, _ := tx.CreateBucket([]byte("games"))
			controllers.CreateTeamGame(createdBucket, "BabyFoot", 2, []string{
				"Matchs de qualification en 2 contre 2",
				"Pas de certificat médical sur cette épreuve",
				"Qualifications :",
				"	-   Lundi 03 Juin,    de 12h15 à 13h45",
				"	-   Mardi 04 Juin,    de 12h15 à 13h45",
				"	-   Mercredi 05 Juin, de 12h15 à 13h45",
				"	-   Jeudi 06 Juin,    de 12h15 à 13h45",
				"Finale : Vendredi 07 Juin de 12h15 à 13h45",
			})
			controllers.CreateIndividualGame(createdBucket, "Badminton", []string{
				"Nouvelle formule : le tournoi surprise",
				"- Tournoi non homologué",
				"- Le tournoi se déroulera en double/mixte.",
				"- Les paires seront tirées au sort aléatoirement à chaque tour.",
				"- On ne rejoue pas avec / contre les mêmes personnes",
				"- Pas de pair homme contre une paire femme.",
				"- Les matchs durent 7 minutes, le coup d’envoi et la fin du match sont signalés par un buzzer.",
				"- Le point disputé au moment du buzzer ne compte pas.",
				"- Si un partenaire est absent au bout d'une minute, une personne peut être choisie au hasard pour assurer le match (ce match ne compte pas pour cette personne)",
				"Certificat médical et /ou licence obligatoire",
				"Date de l'épreuve : Dimanche 09 Juin de 9h à 18h",
			})
			controllers.CreateTeamGame(createdBucket, "Basketball", 5, []string{
				"Épreuve mixte mais aucune obligation de présence d’une fille dans le cinq entrant sur le terrain",
				"Certificat médical et /ou licence obligatoire",
				"Qualifications :",
				"	-   Lundi 20 Mai,    de 17h30 à 21h30",
				"	-   Mardi 21 Mai,    de 17h30 à 21h30",
				"	-   Mercredi 22 Mai, de 17h30 à 21h30",
				"	-   Jeudi 23 Mai,    de 17h30 à 21h30",
				"	-   Vendredi 24 Mai, de 17h30 à 21h30",
				"Demi-Finales : Lundi 27 Mai de 17h30 à 21h30",
				"Finale : Mercredi 29 Mai de 18h00 à 21h30",
			})
			controllers.CreateTeamGame(createdBucket, "Beach / Grass Volley", 3, []string{
				"Tournoi mixte sur gazon (pour remplacer sur sable cet année) en 3 x 3,",
				"Certificat médical et /ou licence obligatoire",
				"Date de l'épreuve : Samedi 08 Juin de 09h00 à 18h00",
			})
			controllers.CreateTeamGame(createdBucket, "Belote Contrée", 2, []string{
				"Pas de certificat médical et/ou licence obligatoire (heureusement)",
				"Qualifications :",
				"	-   Lundi 20 Mai,    de 18h00 à 21h00",
				"	-   Mercredi 22 Mai, de 18h00 à 21h00",
				"Finale : Vendredi 18 Mai de 18h00 à 21h00",
			})
			controllers.CreateTeamGame(createdBucket, "Boules Carrées", 2, []string{
				"Épreuve disputée en doublettes formées (2 joueurs, pas forcément d'une même entreprise/école)",
				"Pas de certificat médical sur cette épreuve",
				"Dates des épreuves : Samedi 15 Juin de 09h00 à 17h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Bowling", []string{
				"2 parties (pas de possibilité de rejouer) - Le meilleur score des 2 parties est retenu",
				"Pas de certificat médical",
				"Qualifications :",
				"	-   Mardi 21 Mai,     de 18h00 à 20h00",
				"	-   Jeudi 23 Mai,     de 12h00 à 14h00",
				"	-   Mardi 28 Mai,     de 12h00 à 14h00",
				"	-   Mardi 28 Mai,     de 18h00 à 20h00",
				"	-   Mardi 04 Juin,    de 18h00 à 20h00",
				"	-   Jeudi 06 Juin,    de 12h00 à 14h00",
				"Finale : Mardi 11 Juin de 18h00 à 20h00",
			})
			controllers.CreateTeamGame(createdBucket, "Course Orientation", 2, []string{
				" 2 parcours pédestres au choix :",
				"- Un parcours DÉCOUVERTE, qualifié de facile. Pas de classement pour les Jeux de Sophia.",
				"- Un parcours COMPÉTITION, qualifié de plus physique, plus technique. Avec un classement pour les Jeux de Sophia.",
				"Certificat médical et/ou licence obligatoire",
				"Attention, épreuve populaire qui peut être complète avant la fin des inscriptions",
				"Date de l'épreuve : Jeudi 13 Juin de 17h00 à 20h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Cross", []string{
				"Cross 2 distances à parcourir : 3 km ou 9 km",
				"Attention, l’épreuve se déroule en même temps que le trail, il n’est donc pas possible de participer aux deux épreuves.",
				"Certificat médical et/ou licence obligatoire",
				"Date de l'épreuve : Mardi 21 Mai de 17h00 à 20h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Cross Marche", []string{
				"Marche (classique, active ou nordique) de 3 km",
				"Certificat médical et/ou licence obligatoire",
				"Date de l'épreuve : Mardi 21 Mai de 17h00 à 20h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Danse", []string{
				"Petits challenges ludiques à relever pour ceux qui le souhaitent !",
				"Vous pouvez inviter du monde EXTÉRIEUR aux JEUX DE SOPHIA !",
				"Pour les non-inscrits aux Jeux de Sophia, tarif de 10€ par personne payable sur place ou en ligne avec weezevent",
				"Certificat médical et/ou licence obligatoire",
				"Date de l'épreuve : Jeudi 13 Juin de 17h00 à 20h00",
			})
			controllers.CreateTeamGame(createdBucket, "E-Sport", 2, []string{
				"32 Equipes de 2 personnes (équipes MIXTES) sur Counter Strike",
				"Pas de certificat médical pour cette épreuve",
				"Date de l'épreuve : Samedi 15 Juin de 09h00 à 18h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Echecs", []string{
				"Les echecs reviennent en force pour cette nouvelle édition !!!",
				"Pas de certificat médical pour cette épreuve",
				"Date de l'épreuve : Samedi 07 Juin de 18h00 à 22h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Escalade", []string{
				"Pour la première année, l’escalade de bloc est présente en sport de démonstration aux Jeux de Sophia 2019.",
				"En conséquence, pas de réelle compétition, si ce n’est contre soi-même.",
				"Pas de certificat médical",
				"Sessions :",
				"	-   Mardi 11 Juin,    de 12h15 à 13h45",
				"	-   Jeudi 13 Juin,    de 18h30 à 20h00",
			})
			controllers.CreateTeamGame(createdBucket, "Fléchettes", 2, []string{
				"But : être l'équipe à marquer le plus rapidement 301 ou 501 points, 3 coups par joueurs",
				"Pas de certificat médical",
				"Qualifications :",
				"	-   Mercredi 29 Mai,  de 18h00 à 21h00",
				"	-   Lundi 03 Juin,    de 18h00 à 22h00",
				"Date des finales : Mercredi 30 mai de 18h00 à 21h00",
			})
			controllers.CreateTeamGame(createdBucket, "Football à 5", 5, []string{
				"Equipe de 5 sur le terrain avec possibilité d'avoir 2 remplaçants max",
				"Certificat médical et/ou licence obligatoire",
				"Épreuve spéciale : 9€ de supplément par personne",
				"	-   Lundi 20 Mai,    de 12h00 à 14h00",
				"	-   Mardi 21 Mai,    de 12h00 à 14h00",
				"	-   Mercredi 22 Mai, de 12h00 à 14h00",
				"	-   Jeudi 23 Mai,    de 12h00 à 14h00",
				"	-   Vendredi 24 Mai, de de 12h00 à 14h00",
				"Date des finales : Dimanche 26 Mai de 10h00 à 14h00.",
			})
			controllers.CreateTeamGame(createdBucket, "Football Féminin", 7, []string{
				"Équipes de 7 joueuses avec possibilité d'avoir 2 joueuses remplaçantes supplémentaires",
				"Certificat médical et/ou licence obligatoire",
				"Date de l'épreuve: Jeudi 14 Juin de 18h00 à 22h00",
			})
			controllers.CreateTeamGame(createdBucket, "Football Masculin", 7, []string{
				"Equipes de 7 joueurs avec possibilité d'avoir 3 joueurs remplaçants supplémentaires",
				"Certificat médical et/ou licence obligatoire",
				"Qualifications :",
				"	-   Vendredi 24 Mai,  de 18h00 à 19h00",
				"	-   Vendredi 24 Mai,  de 19h00 à 20h00",
				"	-   Mercredi 29 Mai,  de 18h00 à 19h00",
				"	-   Mercredi 29 Mai,  de 19h00 à 20h00",
				"	-   Lundi 03 Juin,    de 18h00 à 19h00",
				"	-   Lundi 03 Juin,    de 19h00 à 20h00",
				"Date des finales : Samedi 08 juin 2018 de 09h00 à 15h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Geocaching", []string{
				"Cette année, on inverse les rôles ! Cacher une géocache est une grande étape dans la vie de tout géocacheur. A vous de créer, cacher et trouver !",
				"Nouveauté, l'épreuve est organisée en deux parties :",
				"Création de géocaches du 20 mai au 9 juin inclus (participation facultative, possibilité de créer une géocache en équipe)",
				"Recherche des géocaches du 27 mai au 20 juin",
				"Nous organiserons aussi un événement Géocaching officiel (date restant à définir semaine du 20 mai) pour répondre à toutes vos questions sur la création de géocaches",
				"Les dates définies sont susceptibles d’être modifiées en fonction du déroulement de l’épreuve",
			})
			controllers.CreateIndividualGame(createdBucket, "Golf", []string{
				"18 trous Stabl. NET & BRUT par équipes",
				"Classement en NET & BRUT / Classement Société : total des trois meilleurs scores en NET en excluant les débutants / Index maxi 54",
				"Certificat médical ET licence obligatoire",
				"Épreuve spéciale : 63€ de supplément par personne",
				"Règlement et conditions assez complexes, voir directement sur le site : http://www.jeuxdesophia.com/jcms/rda_6289/fr/golf",
				"Dates des épreuves : ",
				"   -   Vendredi 24 mai : 10 départs de 3 joueurs (toutes les 9 minutes) de 14h00 à 18h00.",
				"   -   Samedi 25 mai : 25 départs de 3 joueurs (toutes les 9 minutes) de 08h00 à 18h00.",
			})
			controllers.CreateTeamGame(createdBucket, "Handball", 6, []string{
				"Equipes de 6 joueurs (5 joueurs de champ et 1 gardien) avec 2 remplaçants maximum.",
				"Minimum 5 joueurs en début de match avec 1 fille sur le terrain.",
				"Si l’équipe n’a pas de joueuse féminine, elle peut jouer mais le match sera perdu par 10 à 0.",
				"Certificat médical et/ou licence obligatoire",
				"Date des finales : Dimanche 16 Juin de 09h00 à 18h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Karting", []string{
				"Compétition de karting sur les hauteurs de la Côte d'Azur",
				"Certificat médical et/ou licence obligatoire",
				"Épreuve spéciale : 14 € de supplément par personne",
				"Qualifications :",
				"	-   Lundi 20 Mai,     de 18h00 à 20h30",
				"	-   Mardi 21 Mai,     de 18h00 à 20h30",
				"	-   Mercredi 22 Mai,  de 18h00 à 20h30",
				"	-   Lundi 27 Mai,     de 18h00 à 20h30",
				"	-   Mardi 28 Mai,     de 18h00 à 20h30",
				"	-   Mercredi 29 Mai,  de 18h00 à 20h30",
				"Finale : Lundi 03 Juin de 18h00 à 21h00",
			})
			controllers.CreateTeamGame(createdBucket, "Kayak", 2, []string{
				"Kayak bi-places large et assez stable pour les novices",
				"Certificat médical et/ou licence obligatoire",
				"Épreuve spéciale : 14 € de supplément par personne",
				"Epreuve : Samedi 25 mai de 09h00 à 13h00",
			})
			controllers.CreateTeamGame(createdBucket, "Laser Quest", 3, []string{
				"Composition :  Par équipe de 3 personnes, parties de 7 minutes. 5 parties au total.",
				"Certificat médical et/ou licence obligatoire",
				"Épreuve spéciale : 13 € de supplément par personne",
				"Qualifications :",
				"	-   Lundi 20 Mai,     de 17h30 à 19h30",
				"	-   Lundi 20 Mai,     de 20h00 à 22h00",
				"	-   Mercredi 22 Mai,  de 17h30 à 19h30",
				"	-   Mercredi 22 Mai,  de 20h00 à 22h00",
				"	-   Lundi 27 Mai,     de 17h30 à 19h30",
				"	-   Lundi 27 Mai,     de 20h00 à 22h00",
				"	-   Mercredi 29 Mai,  de 17h30 à 19h30",
				"	-   Mercredi 29 Mai,  de 20h00 à 22h00",
				"	-   Lundi 03 Juin,    de 17h30 à 19h30",
				"	-   Lundi 03 Juin,    de 20h00 à 22h00",
				"	-   Mercredi 05 Juin, de 17h30 à 19h30",
				"	-   Mercredi 05 Juin, de 20h00 à 22h00",
				"Finale : Lundi 12 Juin de 17h30 à 22h00",
			})
			controllers.CreateTeamGame(createdBucket, "Padel", 2, []string{
				"4 jeux gagnants",
				"Tiebreak en 6 points à 3-3",
				"No-ad sur tous les matchs",
				"Certificat médical et/ou licence obligatoire",
				"Épreuve spéciale : 8 € de supplément par personne",
				"Qualifications :",
				"	-   Lundi 20 Mai,     de 12h00 à 13h30",
				"	-   Mercredi 22 Mai,  de 12h00 à 13h30",
				"	-   Lundi 27 Mai,     de 12h00 à 13h30",
				"	-   Mercredi 29 Mai,  de 12h00 à 13h30",
				"	-   Mercredi 05 Juin, de 17h30 à 19h30",
				"Finale : Vendredi 07 Juin de 17h30 à 19h30",
			})
			controllers.CreateTeamGame(createdBucket, "Pétanque", 3, []string{
				"Épreuve disputée en triplettes formées (3 joueurs, pas forcément d'une même entreprise/école)",
				"Toutes les équipes sont convoquées à 8h20 pour la vérification des inscriptions, et aussi pour prendre connaissance des adversaires et des terrains.",
				"Pas de certificat médical",
				"Date de l'épreuve : Samedi 08 Juin de 09h00 à 22h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Photomarathon", []string{
				"Voir déroulement de l'épreuve sur le site",
				"Pas de certificat médical",
				"Date de l'épreuve : Jeudi 06 Juin de 18h00 à 22h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Poker", []string{
				"Tournoi de Texas Hold’em No Limit",
				"Pas de certificat médical",
				"Qualifications :",
				"	-   Mardi 28 Mai,     de 18h30 à 23h00",
				"	-   Lundi 03 Juin,    de 18h30 à 23h00",
				"	-   Mardi 12 Juin,    de 18h30 à 23h00",
				"Finale : Vendredi 14 Juin de de 18h30 à 23h00",
			})
			controllers.CreateTeamGame(createdBucket, "Rugby / Tag Rugby", 7, []string{
				"Journée découverte rugby: Rugby à VII (règles loisir) et tag-rugby (sans contact)",
				"Equipes de 7 joueurs + 2 remplaçants max",
				"Les équipes féminines et mixtes sont les bienvenues !",
				"Matchs de 2x7 min sur un terrain de 35 x 50 m",
				"Certificat médical et/ou licence obligatoire",
				"Date de l'épreuve : Dimanche 16 Juin de 09h00 à 15h00",
			})
			controllers.CreateTeamGame(createdBucket, "SophiAdventure", 5, []string{
				"Par équipe de 4 (ou 5)",
				"A pied et en 3 heures MAX : environ 1h de marche et 2h de jeu",
				"Chaque épreuve rapporte des points pour le classement final ;",
				"Des énigmes permettent de débloquer DES INDICES.",
				"Pas de certificat médical",
				"Date de l'épreuve : Jeudi 06 Juin de 18h00 à 21h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Squash", []string{
				"Equipe de 2 personnes",
				"1 rencontre entre équipes = 30 minutes",
				"Épreuve spéciale : 6 € de supplément par personne ",
				"Certificat médical et/ou licence obligatoire",
				"Qualifications : ",
				"	-   Jeudi 23 Mai,         de 18h30 à 21h30",
				"	-   Vendredi 24 Mai,      de 18h30 à 21h30",
				"	-   Lundi 27 Mai,         de 18h30 à 21h30",
				"	-   Lundi 04 Juin,        de 18h30 à 21h30",
				"Finale : Jeudi 06 Juin 18h30 à 21h30",
			})
			controllers.CreateTeamGame(createdBucket, "Tennis", 2, []string{
				"Format similaire à la Coupe Davis : Tournoi Equipes de 2 à votre convenance (H/H, F/F, mixte)",
				"Certificat médical et/ou licence obligatoire",
				"Épreuve spéciale : 15 € de supplément par personne ",
				"Qualifications : ",
				"	-   Lundi 27 Mai,         de 18h00 à 22h00",
				"	-   Lundi 03 Juin,        de 18h00 à 22h00",
				"	-   Mercredi 05 Juin,     de 18h00 à 22h00",
				"	-   Mercredi 12 Juin,     de 18h00 à 22h00",
				"Finale : Samedi 15 Juin de 13h00 à 20h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Tennis de Table", []string{
				"Tournois individuels (Féminin, Homme non classé et classé)",
				"Certificat médical et/ou licence obligatoire",
				"Date de l'épreuve : Dimanche 02 Juin de 9h à 18h",
				"	-   Tournoi non classé mixte de 08h30 à 13h00",
				"	-   Tournoi non classé feminin de 13h30 à 17h00",
				"	-   Tournoi classé mixte de 13h30 à 18h00",
			})
			controllers.CreateIndividualGame(createdBucket, "Tir à l'Arc", []string{
				"Session de tir à l'arc",
				"Épreuve spéciale : supplément non précisé",
				"Dates des épreuves : Voir directement sur le site",
			})
			controllers.CreateTeamGame(createdBucket, "Tir à l'Arc par Equipe", 4, []string{
				"Session de tir à l'arc et Arc'trap (loisir)",
				"Les équipes sont constituées de 4 hommes ou 4 femmes. Pas de mixité.",
				"Les membres de l'équipe sont composés de 2 membres maximum d'une même entreprise.",
				"Épreuve spéciale : supplément non précisé",
				"Dates des qualifications : Voir directement sur le site",
			})
			controllers.CreateIndividualGame(createdBucket, "Tir à l'Arc Loisir", []string{
				"Arc'trap loisir",
				"Épreuve spéciale : supplément non précisé",
				"Sessions :",
				"	-   Mercredi 22 Mai,  de 17h30 à 18h15",
				"	-   Mercredi 22 Mai,  de 18h15 à 19h00",
				"	-   Jeudi 23 Mai,     de 12h15 à 13h15",
			})
			controllers.CreateIndividualGame(createdBucket, "Trail", []string{
				"Au choix :",
				"- Epreuve courte fait 10km pour 400 m de dénivelé positif.",
				"- L’épreuve longue fait 15km pour 500m de dénivelé positif.",
				"Certificat médical et/ou licence obligatoire",
				"Date de l'épreuve : Mardi 11 Juin de 17h00 à 20h00",
			})
			controllers.CreateTeamGame(createdBucket, "Ultimate Frisbee Compet", 5, []string{
				"Equipes mixtes : 5 joueurs (minimum 1 fille) + 3 remplacants possibles",
				"Certificat médical et/ou licence obligatoire",
				"Date de l'épreuve : Mardi 16 Juin de 09h00 à 15h00",
			})
			controllers.CreateTeamGame(createdBucket, "Ultimate Frisbee Loisir", 5, []string{
				"Equipe de 6 joueurs (2 remplaçants acceptés)",
				"Certificat médical et/ou licence obligatoire",
				"Sessions : ",
				"	-   Mercredi 22 Mai,     de 17h30 à 21h00",
				"	-   Vendredi 24 Mai,     de 17h30 à 21h00",
				"	-   Mardi 28 Mai,        de 17h30 à 21h00",
				"	-   Mercredi 29 Mai,     de 17h30 à 21h00",
				"	-   Lundi 03 Juin,       de 17h30 à 21h00",
				"	-   Mardi 04 Juin,       de 17h30 à 21h00",
			})
			controllers.CreateTeamGame(createdBucket, "Urban Game", 3, []string{
				"Equipe de 3 à 5 joueurs",
				"Pas de certificat médical",
				"Sessions : ",
				"	-   Lundi 03 Juin,        de 18h00 à 22h00",
				"	-   Mercredi 05 Juin,     de 18h00 à 22h00",
				"	-   Vendredi 07 Juin,     de 18h00 à 22h00",
				"Finale : Mercredi 12 Juin de 18h00 à 22h00",
			})
			controllers.CreateTeamGame(createdBucket, "Voile", 2, []string{
				"Régate se déroulant sur des catamarans de type Hoby Cat 16, avec équipage de 2 personnes",
				"Certificat médical et/ou licence obligatoire",
				"Épreuve spéciale : 27€ de supplément par personne ",
				"Date de l'épreuve : Dimanche 09 Juin de 9h00 à 18h00",
			})
			controllers.CreateTeamGame(createdBucket, "VolleyBall", 6, []string{
				"Equipe de 6 joueurs (3 remplaçants acceptés)",
				"Certificat médical et/ou licence obligatoire",
				"Qualifications : ",
				"	-   Lundi 03 Juin,        de 18h00 à 22h00",
				"	-   Mercredi 05 Juin,     de 18h00 à 22h00",
				"	-   Vendredi 07 Juin,     de 18h00 à 22h00",
				"Finale : Mercredi 12 Juin de 18h00 à 22h00",
			})
			controllers.CreateIndividualGame(createdBucket, "VTT 15 km", []string{
				"Parcours compétition et parcours loisir encadrés - Catégorie Hommes et Femmes",
				"Certificat médical et/ou licence obligatoire",
				"Date de l'épreuve : Mardi 04 Juin de 17h00 à 20h00",
			})
			controllers.CreateTeamGame(createdBucket, "VTT Nocturne Relais", 2, []string{
				"Épreuve VTT en relai sur une boucle de 4 km",
				"Certificat médical et/ou licence obligatoire",
				"Date de l'épreuve : Jeudi 23 Mai de 21h00 à 23h00",
			})
		}
		return nil
	})

	controllers.SetDB(db)

	defer db.Close()

	rand.Seed(time.Now().UnixNano())

	beego.Router("/", &controllers.HomeController{})
	beego.Router("/signin", &controllers.HomeController{}, "post:SignInQuery")
	beego.Router("/signup", &controllers.HomeController{}, "post:SignUpQuery")

	beego.Router("/games/*", &controllers.DataController{}, "get:Get")

	beego.Router("/submitPlayerData", &controllers.DataController{}, "post:SubmitPlayerData")

	beego.Router("/addPlayerToGame", &controllers.DataController{}, "post:AddPlayerToGame")
	beego.Router("/removePlayerFromGame", &controllers.DataController{}, "post:RemovePlayerFromGame")
	beego.Router("/submitPlayerGameComment", &controllers.DataController{}, "post:SubmitPlayerGameComment")

	beego.Router("/submitGameNewPost", &controllers.DataController{}, "post:SubmitGameNewPost")
	beego.Router("/submitGameModifyPost", &controllers.DataController{}, "post:SubmitGameModifyPost")
	beego.Router("/submitGameDeletePost", &controllers.DataController{}, "post:SubmitGameDeletePost")
	beego.Router("/restoreGamePost", &controllers.DataController{}, "post:RestoreGamePost")

	beego.Router("/addTeamToGame", &controllers.DataController{}, "post:AddTeamToGame")
	beego.Router("/removeTeamFromGame", &controllers.DataController{}, "post:RemoveTeamFromGame")
	beego.Router("/changeTeamName", &controllers.DataController{}, "post:ChangeTeamName")
	beego.Router("/changeManager", &controllers.DataController{}, "post:ChangeManager")
	beego.Router("/submitTeamComment", &controllers.DataController{}, "post:SubmitTeamComment")

	beego.Router("/submitTeamNewPost", &controllers.DataController{}, "post:SubmitTeamNewPost")
	beego.Router("/submitTeamModifyPost", &controllers.DataController{}, "post:SubmitTeamModifyPost")
	beego.Router("/submitTeamDeletePost", &controllers.DataController{}, "post:SubmitTeamDeletePost")
	beego.Router("/restoreTeamPost", &controllers.DataController{}, "post:RestoreTeamPost")

	beego.Router("/addPlayerToTeam", &controllers.DataController{}, "post:AddPlayerToTeam")
	beego.Router("/removePlayerFromTeam", &controllers.DataController{}, "post:RemovePlayerFromTeam")
	beego.Router("/submitPlayerTeamComment", &controllers.DataController{}, "post:SubmitPlayerTeamComment")

	beego.Run()
}
